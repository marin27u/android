package fr.Gerphanion.Marin;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;

public class Creation extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creation);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        Button add = (Button)findViewById(R.id.button);

        add.setOnClickListener(new View.OnClickListener(){


            @Override
            public void onClick(View view) {

                EditText text =(EditText) findViewById(R.id.editText);
                String name = text.getText().toString();

                RadioGroup radio = (RadioGroup) findViewById(R.id.radioGroup);


                TodoItem item = new TodoItem(TodoItem.Tags.Important,name);

                TodoDbHelper.addItem(item,getBaseContext());

                finish();
            }
        });
    }



}
